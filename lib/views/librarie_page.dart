import 'dart:io';
import 'package:bleue/models/metadata_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:just_audio/just_audio.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

import '../models/player.dart';

class MusicList extends StatefulWidget {
  const MusicList({Key? key});

  @override
  _MusicListState createState() => _MusicListState();
}

class _MusicListState extends State<MusicList> {
  List<String> musicList = [];
  List<String> musicListPaths = [];
  List<List<String>> musicListMetadatas = [];

  var _isLoading = true;

  @override
  void initState() {
    super.initState();
    getMusicList();
  }

  Future<void> getMusicList() async {
    Future.delayed(Duration(seconds: 1), () async {
      if (!Platform.isAndroid) {
        Directory? musicDirectory = await getApplicationDocumentsDirectory();
        String musicPath = '${musicDirectory.path}/Music';
        if (!await Directory(musicPath).exists()) {
          Directory(musicPath).create().then((Directory directory) {
            print('Directory Music created');
          });
        }
        await getMusicFiles(Directory(musicPath));
      } else {
        if (await Permission.storage.request().isDenied) {
          print("Permission denied");
          var status = await Permission.manageExternalStorage.request();
          if (status.isDenied) {
            print("Permission denied again");
            return null;
          }
        } else {
          Directory? musicDirectory = Directory('/storage/emulated/0/Music');
          await getMusicFiles(musicDirectory);
          Directory? downloadsDirectory = Directory('/storage/emulated/0/Download');
          await getMusicFiles(downloadsDirectory);
        }
      }
      setState(() {
        _isLoading = false;
        Player.instance.musicListName = musicList;
      });
    });
  }
MetadataService controller = Get.put(MetadataService());
  Future<void> getMusicFiles(Directory dir) async {
    List<FileSystemEntity> files = dir.listSync();
    for (FileSystemEntity file in files) {
      if (file is File && file.path.endsWith(".mp3")) {
        var filePath = file.path.split('/');
        musicListPaths.add(file.path);
        var musicName = filePath[filePath.length - 1];
        RegExp exp = RegExp(r'\.mp3$');
        musicName = exp.hasMatch(musicName) ? musicName.replaceAll(exp, '') : musicName;
        musicList.add(musicName);
        var infos = await controller.getMusicMetadatas(file.path);
        musicListMetadatas.add(infos);
      } else if (file is Directory) {
        await getMusicFiles(file);
      }
    }
  }

  Future<bool> _showDeleteConfirmationDialog(String filePath) async {
    return await showDialog<bool>(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text("Confirmation"),
              content: const Text("Voulez-vous vraiment supprimer cette musique ?"),
              actions: <Widget>[
                TextButton(
                  child: const Text("Annuler"),
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                ),
                TextButton(
                  child: const Text("Supprimer"),
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                ),
              ],
            );
          },
        ) ??
        false;
  }

  void _deleteMusicFile(String filePath) async {
    try {
      final file = File(filePath);
      await file.delete();
      setState(() {
        musicListPaths.remove(filePath);
        musicList.removeWhere((name) => name == filePath.split('/').last.split('.').first);
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      body: 
      Column(children: [
        const Padding(
          padding: EdgeInsets.only(left: 20, top: 20, bottom: 20),
          child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                "Bibliothèque",
                style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                    color: Colors.blue),
              )),
        ),
        _isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          :
         Expanded(
           child: Padding(
             padding: const EdgeInsets.only(left: 7),
             child: ListView.builder(
                   itemCount: musicList.length + 1,
                   itemBuilder: (context, index) {
                if (index < musicList.length){
              return ListTile(
                title: Row(children: [ 
                  Container(
                    margin: const EdgeInsets.only(right: 10),
                height: 45,
                width: 45,
                decoration: const BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                child: const Icon(Icons.music_note, size: 30, color: Colors.white),
              ),
                  Flexible(
                    child: Column(
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(musicListMetadatas[index][0] != "" ? musicListMetadatas[index][0] : musicList[index], overflow: TextOverflow.ellipsis,
                        style: const TextStyle(fontSize: 18 ),
                        ),
                      ),
                      musicListMetadatas[index][0] != "" ? Align(
                        alignment: Alignment.centerLeft,
                        child: Text(musicListMetadatas[index][1].toString(), overflow: TextOverflow.ellipsis,
                        style: const TextStyle(fontSize: 14 ),
                        ),
                      ) : Container(width: 0,)
                    ],
                                ),
                  )],),
                trailing: IconButton(
                  icon: const Icon(Icons.delete),
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: const Text("Supprimer la musique ?"),
                          content: Text("Êtes-vous sûr de vouloir supprimer ${musicList[index]} ?"),
                          actions: [
                            TextButton(
                              onPressed: () => Navigator.of(context).pop(),
                              child: const Text("Annuler"),
                            ),
                            TextButton(
                              onPressed: () async {
                                var file = File(musicListPaths[index]);
                                try {
                                  await file.delete();
                                  musicList.removeAt(index);
                                  musicListPaths.removeAt(index);
                                  setState(() {});
                                  Navigator.of(context).pop();
                                } catch (e) {
                                  print(e.toString());
                                }
                              },
                              child: const Text("Supprimer"),
                            ),
                          ],
                        );
                      },
                    );
                  },
                ),
                onTap: () async {        
                  List<AudioSource> musics = [];
                  Player.instance.musicListPath = musicListPaths;
                  for (var i = 0; i < musicListPaths.length; i++) {
                    var filePath = musicListPaths[i];
                    var audioSource = AudioSource.uri(Uri.file(filePath));
                    musics.add(audioSource);
                  }
                    
                  var playlist = ConcatenatingAudioSource(
                    useLazyPreparation: true,
                    shuffleOrder: DefaultShuffleOrder(),
                    children: musics,
                  );
                    
                  await Player.instance.player.setAudioSource(playlist, initialIndex: index, initialPosition: Duration.zero);
                  Player.instance.play();
                  controller.updateStateRadio(false);
                  Player.instance.showFullPlayer(context);
                },
              );}
              else{
                return const SizedBox(height: 60);
              }
                   },
                 ),
           ),
         ),],),
    );
  }
}