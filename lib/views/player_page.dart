import 'package:bleue/component/player_component.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:just_audio/just_audio.dart';

import '../models/metadata_service.dart';
import '../models/player.dart';

class PlayerPage extends StatefulWidget {
  const PlayerPage({super.key});

  @override
  State<PlayerPage> createState() => _PlayerPageState();
}

class _PlayerPageState extends State<PlayerPage> {
  AudioPlayer player = Player.instance.player;
  
  @override
  Widget build(BuildContext context) {
    MetadataService controller = Get.put(MetadataService());
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(child: Container()),
        Center(
          child: Obx(() => Container(
              margin: const EdgeInsets.only(top: 50),
              height: 250,
              width: 250,
              decoration: const BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  boxShadow: [
                    BoxShadow(
                        color: Color.fromARGB(156, 83, 83, 83),
                        blurRadius: 10,
                        spreadRadius: 1,
                        offset: Offset(0, 0))
                  ]
                  ),
              child: controller.playerImage.value.toString() != "" ? ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.network(
                  controller.playerImage.value.toString(),
                  fit: BoxFit.cover,
                ),
              ) : const Icon(Icons.music_note, size: 100, color: Colors.white),
            ),
          ),
        ),
        Expanded(child: Container()),
        Column(children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Container(
            margin: const EdgeInsets.only(top: 50, left: 30),
            child: Obx(() => Text(
                controller.playerTitle.value.toString(),
                style: const TextStyle(fontSize: 20),
              ),
            ),
          ),
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Container(
            margin: const EdgeInsets.only(top: 10, left: 30),
            child: Obx( () => Text(
                controller.playerArtist.value.toString(),
                style: const TextStyle(fontSize: 15),
              ),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 50, top: 20),
          child:
        Player_component(player: player),)
        ],
        ),
        
      ]),
    );
  }
}

