import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:path_provider/path_provider.dart';
import 'dart:io';

class DLPage extends StatefulWidget {
  @override
  _DLPageState createState() => _DLPageState();
}

class _DLPageState extends State<DLPage> {
  List _musicList = [];
  final TextEditingController _searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    fetchMusicList();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  Future<void> fetchMusicList({String query = ''}) async {
    final response = await http.get(Uri.parse(
        'https://api.jamendo.com/v3.0/tracks/?client_id=fe77441b&search=$query'));

    if (response.statusCode == 200) {
      setState(() {
        _musicList = json.decode(response.body)['results'];
      });
    } else {
      throw Exception('Failed to load music list');
    }
  }

  Future<void> downloadMusic(String url, String name) async {
    String musicPath;
    if (!Platform.isAndroid) {
      Directory? musicDirectory = await getApplicationDocumentsDirectory();
      musicPath = '${musicDirectory.path}/Music';
    } else {
      musicPath = '/storage/emulated/0/Music';
    }

    if (!(await Directory(musicPath).exists())) {
      await Directory(musicPath).create(recursive: true);
    }

    final response = await http.get(Uri.parse(url));
    final file = File('$musicPath/$name.mp3');
    await file.writeAsBytes(response.bodyBytes);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const Padding(
            padding: EdgeInsets.only(left: 20, top: 20),
            child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Téléchargement",
                  style: TextStyle(
                      fontSize: 35,
                      fontWeight: FontWeight.bold,
                      color: Colors.blue),
                )),
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: 20, right: 20, top: 8, bottom: 15),
            child: SizedBox(
              height: 50,
              child: TextField(
                controller: _searchController,
                decoration: InputDecoration(
                  hintText: 'Search...',
                  suffixIcon: Container(
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.only(topRight: Radius.circular(20.0), bottomRight: Radius.circular(20.0) ),
                      color: Colors.blue,
                    ),
                    child: IconButton(
                      icon: const Icon(Icons.search, color: Colors.white),
                      onPressed: () {
                        fetchMusicList(query: _searchController.text);
                      },
                    ),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: _musicList.length + 1,
                itemBuilder: (context, index) {
                  if (index < _musicList.length) {
                    final music = _musicList[index];
                    return ListTile(
                      title: Text(music['name'], overflow: TextOverflow.ellipsis,),
                      subtitle: Text(music['artist_name']),
                      trailing: ElevatedButton(
                        child: Text('Download'),
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.blue,
                          ),
                        onPressed: () {
                          downloadMusic(music['audiodownload'], music['name']);
                        },
                      ),
                    );
                  } else {
                    return const SizedBox(height: 60);
                  }
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
