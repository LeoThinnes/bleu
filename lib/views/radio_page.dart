import 'package:bleue/models/metadata_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../models/player.dart';

class RadioPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MetadataService controller = Get.put(MetadataService());
    return Scaffold(
      body: Column(children: [
        const Padding(
          padding: EdgeInsets.only(left: 20, top: 20),
          child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                "Radio",
                style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                    color: Colors.blue),
              )),
        ),
        Expanded(
          child: Center(child: 
          SizedBox(width: 250, height: 250,
            child: ElevatedButton(onPressed: ()async => {
              await Player.instance.player.setUrl('https://listen.radioking.com/radio/242578/stream/286663'),
              controller.updateStateRadio(true),
              Player.instance.showFullPlayer(context),
              MetadataService(),
              Player.instance.play(),              
              }, 
              style : ElevatedButton.styleFrom(
              shape: const CircleBorder(),
              padding: const EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 30),
              backgroundColor: Colors.blue,
              foregroundColor: Colors.white,
              elevation: 10,
            ), child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.headphones, size: 130, color: Colors.white,),
                  Text("Ecouter la radio", style: TextStyle(fontSize: 20),)
                ],
              ),),
          )),
        ),]),
        );
}
}

