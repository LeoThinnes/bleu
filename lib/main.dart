import 'package:bleue/views/download_music_page.dart';
import 'package:bleue/views/librarie_page.dart';
import 'package:bleue/views/radio_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'component/little_player.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Bleu',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: const Color.fromARGB(0, 255, 255, 255)),
        tabBarTheme: const TabBarTheme(labelColor: Colors.blue),
      ),
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          body: Stack(
            children: [
              SafeArea(
                child: TabBarView(
                  children: [
                    Center(child: MusicList()),
                    Center(child: RadioPage()),
                    Center(child: DLPage()),
                  ],
                ),
              ),
              Positioned(
                left: 0,
                right: 0,
                bottom: 0,
                child: little_player(),
              ),
            ],
          ),
          bottomNavigationBar: const BottomAppBar(
            child: TabBar(
              unselectedLabelColor: Colors.grey,
              tabs: [
                Tab(icon: Icon(Icons.library_music)),
                Tab(icon: Icon(Icons.sensors_rounded)),
                Tab(icon: Icon(Icons.playlist_add)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
