import 'dart:async';

import 'package:audio_video_progress_bar/audio_video_progress_bar.dart';
import 'package:bleue/models/player.dart';
import 'package:bleue/models/progress_bar_data.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:just_audio/just_audio.dart';
import 'package:rxdart/rxdart.dart' as Rx;

import '../models/metadata_service.dart';


class Player_component extends StatelessWidget {
  const Player_component({super.key, required this.player});

  final AudioPlayer player;

  Stream<ProgressBarData> get _progressBarDataStream =>
      Rx.Rx.combineLatest3<Duration, Duration, Duration?, ProgressBarData>(
        player.positionStream,
        player.bufferedPositionStream,
        player.durationStream,
        (position, bufferedPosition, duration) => ProgressBarData(
          position,
          bufferedPosition,
          duration ?? Duration.zero,
        ),
      );

  @override
  Widget build(BuildContext context) {
    MetadataService controller = Get.put(MetadataService());
    return Column(children: [
      StreamBuilder<ProgressBarData>(
          stream: _progressBarDataStream,
          builder: (context, snapshot) {
            final progressBarData = snapshot.data;
            return Container(
              margin: const EdgeInsets.only( left: 30, right: 30),
              child: ProgressBar(
              progressBarColor: Colors.blue,
              thumbColor: Colors.transparent,
              timeLabelTextStyle: const TextStyle(fontSize: 10, color: Colors.blue),
              progress: progressBarData?.progress ?? Duration.zero,
              total: progressBarData?.duration ?? Duration.zero,
              onSeek: player.seek,
            ),);
          }),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Obx(() => Padding(
              padding: const EdgeInsets.only(left: 30),
              child: !controller.isRadioPlayingObs.value ? IconButton(
                  onPressed: () {player.seekToPrevious();},
                  padding: const EdgeInsets.all(0),
                  icon: const Icon(
                    Icons.fast_rewind_rounded,
                    color: Colors.blue,
                    size: 50,
                  )) : Text("")
            ),
          ),
          StreamBuilder(
              stream: player.playerStateStream,
              builder: (context, snapshot) {
                final playerState = snapshot.data;
                final processingState = playerState?.processingState;
                final playing = playerState?.playing;

                if (!(playing ?? false)) {
                  return IconButton(
                      onPressed: player.play,
                      padding: const EdgeInsets.all(0),
                      icon: const Icon(
                        Icons.play_arrow_rounded,
                        color: Colors.blue,
                        size: 50,
                      ));
                } else if (processingState != ProcessingState.completed) {
                  return IconButton(
                      onPressed: player.pause,
                      padding: const EdgeInsets.all(0),
                      icon: const Icon(
                        Icons.pause_rounded,
                        color: Colors.blue,
                        size: 50,
                      ));
                }

                return const Icon(Icons.play_arrow_rounded,
                    color: Colors.blue, size: 50);
              }),
              Obx(() => Padding(
                  padding: const EdgeInsets.only(right: 30),
                  child: !controller.isRadioPlayingObs.value ? IconButton(
                  onPressed: () {player.seekToNext();},
                  padding: const EdgeInsets.all(0),
                  icon: const Icon(
                    Icons.fast_forward_rounded,
                    color: Colors.blue,
                    size: 50,
                  )): Text(""),
                ),
              ),
        ],
      )
    ]);
  }
}
