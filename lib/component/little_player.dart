import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:just_audio/just_audio.dart';

import '../models/player.dart';
import '../models/metadata_service.dart';

class little_player extends StatelessWidget {
   var player = Player.instance.player;

  MetadataService controller = Get.put(MetadataService());
  @override
  Widget build(BuildContext context) {
    return  GestureDetector(
        onTap: () {
          Player.instance.showFullPlayer(context);
        },child: Container(
          decoration: const BoxDecoration(
            color: Color.fromARGB(255, 255, 255, 255),
            boxShadow: [
              BoxShadow(
                color: Color.fromARGB(156, 83, 83, 83),
                blurRadius: 10,
                spreadRadius: 1,
                offset: Offset(0, 0)
              )]
          ),
      height: 60,
      
      child: Center(
        child: Row(
          children: [
            Obx(
              () => Container(
                margin: const EdgeInsets.only(left: 20),
                height: 50,
                width: 50,
                decoration: const BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                child: controller.playerImage.value != ""
                    ? ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.network(
                          controller.playerImage.value,
                          fit: BoxFit.cover,
                        ),
                      )
                    : const Icon(Icons.music_note, size: 30, color: Colors.white),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(left: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: 160,
                    child: Obx(
                      () =>Text(
                        controller.playerTitle.toString(),
                        style: const TextStyle(fontSize: 15), overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(child: Container()),
            StreamBuilder(
                stream: player.playerStateStream,
                builder: (context, snapshot) {
                  final playerState = snapshot.data;
                  final processingState = playerState?.processingState;
                  final playing = playerState?.playing;

                  if (!(playing ?? false)) {
                    return IconButton(
                        onPressed: player.play,
                        padding: const EdgeInsets.all(0),
                        icon: const Icon(
                          Icons.play_arrow_rounded,
                          color: Colors.blue,
                          size: 40,
                        ));
                  } else if (processingState != ProcessingState.completed) {
                    return IconButton(
                        onPressed: player.pause,
                        padding: const EdgeInsets.all(0),
                        icon: const Icon(
                          Icons.pause_rounded,
                          color: Colors.blue,
                          size: 40,
                        ));
                  }

                  return const Icon(Icons.play_arrow_rounded,
                      color: Colors.blue, size: 40);
                }),
            Obx(() => Padding(
                padding: const EdgeInsets.only(right: 10),
                child: !controller.isRadioPlayingObs.value
                    ? IconButton(
                        onPressed: () {
                          player.seekToNext();
                        },
                        padding: const EdgeInsets.all(0),
                        icon: const Icon(
                          Icons.fast_forward_rounded,
                          color: Colors.blue,
                          size: 40,
                        ))
                    : const Text(""),
              ),
            ),
          ],
        ),
      ),
    ),);
  }
}
