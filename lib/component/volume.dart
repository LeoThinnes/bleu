import 'package:flutter/material.dart';

class VolumBar extends StatefulWidget {
  const VolumBar({super.key});

  @override
  State<VolumBar> createState() => _VolumBarState();
}

class _VolumBarState extends State<VolumBar> {
  double _volumeLevel = 0.2;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 20, right: 20, bottom: 30, top: 20),
      child: Row(
        children: [
          const Icon(Icons.volume_off_rounded, color: Colors.blue),
          Expanded(
            child: Slider(
              value: _volumeLevel,
              label: _volumeLevel.round().toString(),
              activeColor: Colors.blue,
              onChanged: (double value) {
                setState(() {
                  _volumeLevel = value;
                });
              },
            ),
          ),
          const Icon(Icons.volume_up_rounded, color: Colors.blue),
        ],
      ),
    );
  }
}
