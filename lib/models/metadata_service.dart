import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:bleue/models/player.dart';
import 'package:get/get.dart';
import 'package:flutter_media_metadata/flutter_media_metadata.dart';

class MetadataService extends GetxController{
  static final MetadataService instance = MetadataService();
  late Map<String, dynamic> _radioInfos;
  Timer? _timer;

  var retriever = MetadataRetriever();

  MetadataService() {
    _radioInfos = {};
    _fetchTrackInfo();
    _timer = Timer.periodic(Duration(seconds: 10), (_) => _fetchTrackInfo());
  }

  var isRadioPlayingObs = false.obs;

  updateStateRadio(bool isRadioPlaying) {
    isRadioPlayingObs.value = isRadioPlaying;
  }

  var playerImage = "".obs;
  var playerTitle = "Musique".obs;
  var playerArtist = "Artiste".obs;

 void setValues(RxString image, RxString artiste, RxString titre) {
    playerImage.value = image.toString();
    playerTitle.value = titre.toString();
    playerArtist.value = artiste.toString();
  }

  Future<void> _fetchTrackInfo() async {
    if (isRadioPlayingObs.value) {
      final response = await http.get(Uri.parse('https://api.radioking.io/widget/radio/bankable-radio/track/current'));
      if (response.statusCode == 200) {
        _radioInfos = jsonDecode(response.body);
        setValues(RxString(_radioInfos['cover'] ?? ""), RxString(_radioInfos['artist'] ?? " "), RxString(_radioInfos['title'] ?? " "));
        print(_radioInfos['title']);
      } else {
        throw Exception('Failed to load track info');
      }   
    }
  }

  Future<void> getMusicFileMetadatas() async{
    var index = await Player.instance.player.currentIndexStream.first;
    var path = Player.instance.musicListPath[index!];
    Metadata metadata = await MetadataRetriever.fromFile(File(path));
    print(Player.instance.musicListName[index]);
    setValues(RxString(""), RxString(metadata.trackArtistNames?[0] ?? "Artiste inconnu" ), RxString(metadata.trackName ?? Player.instance.musicListName[index]));
  }

Future<List<String>> getMusicMetadatas(String path) async{
    Metadata metadata = await MetadataRetriever.fromFile(File(path));
    return [metadata.trackName ?? "", metadata.trackArtistNames?[0] ?? ""];
  }
  void dispose() {
    _timer?.cancel();
  }
}