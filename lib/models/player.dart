import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:just_audio/just_audio.dart';

import '../views/player_page.dart';
import 'metadata_service.dart';

class Player{
  static final Player instance = Player();

  MetadataService controller = Get.put(MetadataService());

  Player(){
    player.currentIndexStream.listen((index) {
      print(index);
      controller.getMusicFileMetadatas();
    });
  }

  final player = AudioPlayer();

  Future<void> play() async {
    await player.play();
  }

  

  var musicListPath = [];
  var musicListName = [];

    void showFullPlayer(BuildContext context) {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0),
                ),
              ),
      builder: (BuildContext context) {
        return Container(
          height: MediaQuery.of(context).size.height * 0.9,
          margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05),
          child: PlayerPage(),
        );
      },
    );
  }
}