
class ProgressBarData{
  const ProgressBarData(
    this.progress,
    this.bufferedProgress,
    this.duration,
  );

  final Duration progress;
  final Duration bufferedProgress;
  final Duration duration;
}